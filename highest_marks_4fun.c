#include<stdio.h>

void input(int a[10][10])
{
    int i,j;
    for(i=0;i<5;i++)
    {
        printf("Enter %d student marks list :\n",i+1);
        for(j=0;j<3;j++)
        {
            printf("Enter %d subject marks : ",j+1);
            scanf("%d",&a[i][j]);
        }
    }
}

void output(int i,int j,int mm)
{
    printf("Highest marks in subject %d is %d by student %d\n",j+1,mm,i+1);
}

void process(int a[5][3])
{
    int i,j,mm=-99;
    for(j=3;j<5;j++)
    {
        for(i=0;i<5;i++)
        {
            if(mm<a[i][j])
            mm=a[i][j];
        }
        output(i,j,mm);
        mm=-99;
    }
}

int main()
{
    int a[10][10];
    input(a);
    process(a);
    return 0;
}