#include<stdio.h>

void input(int a[5][3])
{
    int i,j;
    for(i=0;i<5;i++)
    {
        printf("Enter %d student marks,\n\n",i+1);
        for(j=0;j<3;j++)
        {
            printf("Enter %d subject marks: ",j+1);
            scanf("%d",&a[i][j]);
        }
    }
}

void process(int a[5][3],int h[3])
{
    
	int i,j;
	h[0]=a[0][0];
	h[1]=a[0][1];
	h[2]=a[0][2];
    for(j=0;j<3;j++)
    {
        for(i=0;i<5;i++)
        {
            if(a[i][j]>h[j])
                h[j]=a[i][j];
        }
    }
}

void ouput(int h[3])
{
    int i;
    for(i=0;i<3;i++)
    printf("Highest marks of %d subject is %d\n",i+1,h[i]);
}

int main()
{
    int a[5][3],h[3];
    input(a);
    process(a,h);
    output(h);
    return 0;
}

