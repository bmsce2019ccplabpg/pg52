#include <stdio.h>
int num(int);

int main()
{
    int a,flag;
    printf("Enter any number: ");
    scanf("%d",&a);
    flag=num(a);
    if(flag==1)
        printf("%d is a even number\n",a);
    else
        printf("%d is a odd number\n",a);
	return 0;
}

int num(int a)
{
    if(a%2==0)
        return 1;
    else
        return 0;
}