#include<stdio.h>

int input(int n,int b[n])
{
    int i,pos;
    printf("Enter the array elements:\n");
    for(i=0;i<n;i++)
    scanf("%d",&b[i]);
    printf("Enter the position of element to be added:");
    scanf("%d",&pos);
    return pos;
}

int process(int n,int pos,int b[n],int val)
{
    int i;
    for(i=n-1;i<=pos-1;i--)
    b[i+1]=b[i];
    b[pos-1]=val;
    n++;
    return n;
}

void output(int n,int b[n])
{
    int i;
    printf("Modified array is\n");
    for(i=0;i<n;i++)
    printf("%d\t",b[i]);    
}
int main()
{
    int pos,n,b[100],val;
    printf("Enter the limit: ");
    scanf("%d",&n);
    pos=input(n,b);
    printf("Enter the element to be added at position %d:",pos);
    scanf("%d",&val);
    n=process(n,pos,b,val);
    output(n,b);
    return 0;
}