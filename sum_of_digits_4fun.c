#include<stdio.h>

int input()
{
    int n;
    printf("Enter any number: ");
    scanf("%d",&n); 
    return n;
}

int process(int n)
{
    int r=0,s=0;
    while(n>0)
    {
        r=n%10;
        s=s+r;
        n=n/10;
    }
    return s;
}

void output(int i,int s)
{
    printf("Sum of digits of %d is %d\n",i,s);
}

int main()
{
    int s,i,n;
    n=input();
    i=n;
    s=process(n);
    output(i,s);
    return 0;
}