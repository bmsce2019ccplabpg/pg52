#include <stdio.h>
#include <math.h>

float value(float,float);
float sum(float,float);

int main()
{
	float n1,d1,n2,d2,a,b,c;
    printf("Enter 1st numerator & denominator: ");
    scanf("%f%f",&n1,&d1);
    printf("Enter 2nd numerator & denominator: ");
    scanf("%f%f",&n2,&d2);
    a=value(n1,d1);
    b=value(n2,d2);
    c=sum(a,b);
    printf("Sum of 2 fraction = %f",c);
    return 0;
}

float value(float n,float d)
{
    float f;
    f=n/d;
    return f;  
}

float sum(float a,float b)
{
    return a+b;
}