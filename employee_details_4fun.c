#include<stdio.h>

struct employee
{
    char name[20],dsgn[20],phno[10];
    float sal;
    int empid;
};

struct employee input()
{
    struct employee e;
    printf("Employee Details:\n");
    printf("Name :");
    scanf("%s",e.name);
    printf("Employee id : ");
    scanf("%d",&e.empid);
    printf("Designistion : ");
    scanf("%s",e.dsgn);
    printf("Salary : ");
    scanf("%f",&e.sal);
    printf("Phone number : ");
    scanf("%s",e.phno);
    return e;
}

void output(struct employee e)
{
    printf("\nEmployee Details is :\n");
    printf("Name :%s \n",e.name);
    printf("ID Number:%d \n",e.empid);
    printf("Designistion : %s \n",e.dsgn);
    printf("Salary :%0.2f \n",e.sal);
    printf("Phonr number :%s \n",e.phno);
}

int main()
{
    struct employee e;
    e=input();
    output(e);
    return 0;
}
