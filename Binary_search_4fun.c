#include<stdio.h>
#include<stdlib.h>

int input(int n,int a[n])
{
    int i,key;
    printf("Enter the array elements,\n");
    for(i=0;i<n;i++)
    scanf("%d",&a[i]);
    printf("Enter the search element :");
    scanf("%d",&key);
    return key;
}

void output(int key,int mid)
{
    printf("%d is found at position %d",key,mid+1);
}

void process(int n,int a[n],int key)
{
    int low=0,high=n-1,mid;
    while(low<=high)
    {
        mid=(high+low)/2;
        if(a[mid]==key)
        {
            output(key,mid);
            exit(0);
        }
        else
            if(a[mid]>key)
                high=mid-1;
            else
                low=mid+1;
    }
    printf("%d is not found in given array",key);
}

int main()
{
    int n,a[10],key;
    printf("Enter the no. of elements: ");
    scanf("%d",&n);
    key=input(n,a);
    process(n,a,key);
    return 0;
}