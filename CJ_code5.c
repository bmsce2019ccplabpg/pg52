#include <stdio.h>
#include <string.h>

void input(char a[])
{
    printf("Enter any string: ");
    gets(a);
}

int process(char a[])
{
    int i;
    for(i=0;a[i]!='\0';i++);
    return i;
}

void output(int l)
{
    printf("\nLength of given string is %d",l);
}

int main()
{
    char a[100];int l;
    input(a);
    l=process(a);
    output(l);
    return 0;
}
