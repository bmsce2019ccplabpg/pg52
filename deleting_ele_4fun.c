#include<stdio.h>

int input(int n,int b[n])
{
    int i,pos;
    printf("Enter the array elements:\n");
    for(i=0;i<n;i++)
    scanf("%d",&b[i]);
    printf("Enter the position of element to be removed:");
    scanf("%d",&pos);
    return pos;
}

int process(int n,int pos,int b[n])
{
    int i;
    for(i=pos;i<n;i++)
    b[i-1]=b[i];
    n--;
    return n;
}

void output(int n,int b[n])
{
    int i;
    printf("Modified array is\n");
    for(i=0;i<n;i++)
    printf("%d\t",b[i]);    
}
int main()
{
    int pos,n,b[100];
    printf("Enter the limit: ");
    scanf("%d",&n);
    pos=input(n,b);
    n=process(n,pos,b);
    output(n,b);
    return 0;
}