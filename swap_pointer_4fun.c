#include<stdio.h>

int input()
{
    int n;
    printf("Enter a number: ");
    scanf("%d",&n);
    return n;
}

void swap(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}

void output(int a,int b)
{
    printf("After swaping value are %d and %d",a,b);
}

int main()
{
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(a,b);
    return 0;
}