#include <stdio.h>

int add(int,int);
float sum(float,float);
float square(float);

int main()
{
    int a,b;
    float x,y,z;
    printf("Enter two integer number: ");
    scanf("%d%d",&a,&b);
    printf("Enter two floating number: ");
    scanf("%f%f",&x,&y);
    printf("Enter any number: ");
    scanf("%f",&z);
    printf("\nSum of %d and %d = %d",a,b,add(a,b));
    printf("\nSum of %f and %f = %f",x,y,sum(x,y));
    printf("\nSquare of %f is %0.2f\n",z,square(z));
    return 0;
}

int add(int a,int b)
{
    return a+b;
}

float sum(float x,float y)
{
    return x+y;
}

float square(float z)
{
    return z*z;
}