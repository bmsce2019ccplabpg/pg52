#include<stdio.h>

int input();
int compare(int,int,int);
void output(int,int,int,int);
int main()
{
    int a,b,c,big;
    a=input();
    b=input();
    c=input();
    big=compare(a,b,c);
    output(a,b,c,big);
    return 0;
}

int input()
{
    int a;
    printf("Enter a number: ");
    scanf("%d",&a);
    return a;
}

int compare(int a,int b,int c)
{
    int big;
    big=a;
    if(b>a && b>c)
        big=b;
    if(c>a && c>b)
        big=c;
    return big;
}

void output(int a,int b,int c,int big)
{
    printf("Largest of %d,%d and %d is %d\n ",a,b,c,big);
}