#include<stdio.h>

int input(int n,int a[n])
{
    int i,key;
    printf("Enter the array elements,\n");
    for(i=0;i<n;i++)
    scanf("%d",&a[i]);
    printf("Enter the search element :");
    scanf("%d",&key);
    return key;
}

void output(int key,int pos)
{
    printf("%d is found at position %d",key,pos);
}

void process(int n,int a[n],int key)
{
   int i;
   for(i=0;i<n;i++)
   {
       if(a[i]==key)
       output(key,i+1);
   }
    printf("%d is not found in given array\n",key);
}

int main()
{
    int n,a[10],key;
    printf("Enter the no. of elements: ");
    scanf("%d",&n);
    key=input(n,a);
    process(n,a,key);
    return 0;
}