#include<stdio.h>

int input()
{
    int a;
    printf("Enter a number: ");
    scanf("%d",&a);
    return a;
}

int compare(int a,int b,int c)
{
    int big;
    if(a>b && a>c)
        big=a;
    else
        if(b>a && b>c)
            big=b;
        else
            big=c;
    return big;
}

void output(int a,int b,int c,int big)
{
    printf("Largest of %d,%d and %d is %d\n ",a,b,c,big);
}

int main()
{
    int a,b,c,big;
    a=input();
    b=input();
    c=input();
    big=compare(a,b,c);
    output(a,b,c,big);
    return 0;
}

