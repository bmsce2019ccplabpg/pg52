#include<stdio.h>

void input(int x[10][10],int r,int c)
{
    int i,j;
    printf("Enter the matrix element: \n");
    for(i=0;i<r;i++)
        for(j=0;j<c;j++)
            scanf("%d",&x[i][j]);
    printf("Given matrix is of the form,\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d\t",x[i][j]);
        }
        printf("\n");
    }
}

void process(int a[10][10],int b[10][10],int s[10][10],int r1,int c1)
{
    int i,j;
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
            s[i][j]=a[i][j]-b[i][j];
        }
    }
}

void output(int s[10][10],int r1,int c1)
{
    int i,j;
    printf("Resultant matix after subtraction of given two matrices is \n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
            printf("%d\t",s[i][j]);
        }
        printf("\n");
    }
}

int main()
{
    int a[10][10],b[10][10],s[10][10],r1,r2,c1,c2;
    printf("Enter the rows & colms of first matrix: ");
    scanf("%d%d",&r1,&c1);
    input(a,r1,c1);
    printf("Enter the rows & colms of second matrix: ");
    scanf("%d%d",&r2,&c2);
    input(b,r2,c2);
    if(r1!=r2 || c1!=c2)
    {
        printf("Subtraction is not possible.");
        exit(0);
    }
    else
    {
        process(a,b,s,r1,c1);
        output(s,r1,c1);
    }
    return 0;
}