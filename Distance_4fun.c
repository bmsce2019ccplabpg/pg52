#include <stdio.h>
#include <math.h>
struct point
{
    float x,y;
};

struct point input()
{
    struct point d;
    scanf("%f%f",&d.x,&d.y);
    return d;
}

float process(struct point d1,struct point d2)
{
    float d;
    d=sqrt(pow((d2.x-d1.x),2)+pow((d2.y-d1.y),2));
    return d;
}

void output(float s)
{
    printf("The distance between given points is %f",s);
}

int main()
{
    struct point d1,d2;
    float s;
	printf("Enter the co-ordinates of point 1:");
    d1=input();
    printf("Enter the co-ordinates of point 2:");
    d2=input();
    s=process(d1,d2);
    output(s);
	return 0;
}
