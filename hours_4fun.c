#include<stdio.h>
struct time
{
    int h,m;
};

struct time input()
{
    struct time t;
    printf("Enter time(in hours & min): ");
    scanf("%d%d",&t.h,&t.m);
    return t;
}

int process(struct time t)
{
    return (t.h*60+t.m);
}

void output(struct time t,int m)
{
    printf("Total minutes in %dhr:%dmin is %d",t.h,t.m,m);
}

int main()
{
    struct time t;
    int m;
    t=input();
    m=process(t);
    output(t,m); 
    return 0;
}
