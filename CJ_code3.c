#include<stdio.h>

int input()
{
    int n;
    printf("Enter any integer :");
    scanf("%d",&n);
    return n;
}

int add(int n1,int n2)
{
    if(n1<0)
       n1=n1*-1;
    if(n2<0)
        n2=n2*-1;
    return(n1+n2);
}

void output(int n1,int n2,int n)
{
    printf("The absolute sum of %d and %d is %d\n",n1,n2,n);
}

int main()
{
    int n1,n2,n;
    n1=input();
    n2=input();
    n=add(n1,n2);
    output(n1,n2,n);
    return 0;
}