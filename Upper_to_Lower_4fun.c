#include<stdio.h>

void input(char str[10])
{
    printf("Enter any string: ");
    scanf("%s",str);
}

void process(char str[10],char s[10])
{
    int i=0,j=0;
    while(str[i]!='\0')
    {
        if(str[i]>='a'&&str[i]<='z')
            s[j]=str[i]-32;
        else
            s[j]=str[i]+32;
        j++;i++;
    }
    s[j]='\0';
}

void output(char s[10])
{
    printf("Resultant string is %s",s);
}

int main()
{
    char str[10],s[10];
    input(str);
    process(str,s);
    output(s);
    return 0;
}