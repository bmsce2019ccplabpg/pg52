#include<stdio.h>

int input()
{
    int x;
    printf("Enter any decimal number: ");
    scanf("%d",&x);
    return x;
}

int process(int x,int a[])
{
    int i=0;
    while(x>0)
    {
        a[i]=x%2;
        i++;
        x=x/2;
    }
    return i;
}

void output(int n,int a[n],int x)
{
    int i;
    printf("The binary equalent of %d is ",x);
    for(i=0;i<n;i++)
    printf("%d ",a[i]);
}

int main()
{
    int n,x,a[100];
    x=input();
    n=process(x,a);
    output(n,a,x);
    return 0;
}