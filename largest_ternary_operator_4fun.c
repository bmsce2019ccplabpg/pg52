#include<stdio.h>

int input()
{
    int a;
    printf("Enter a number: ");
    scanf("%d",&a);
    return a;
}

int compare(int a,int b,int c)
{
    int big;
    big=a>b?(a>c?a:c):(b>c?b:c);   
    return big;
}

void output(int a,int b,int c,int big)
{
    printf("Largest of %d,%d and %d is %d\n ",a,b,c,big);
}

int main()
{
    int a,b,c,big;
    a=input();
    b=input();
    c=input();
    big=compare(a,b,c);
    output(a,b,c,big);
    return 0;
}

