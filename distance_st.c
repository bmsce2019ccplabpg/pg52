#include <math.h>
#include <stdio.h>

struct point
{
    float x,y;
};

int main()
{
    struct point d1,d2;
    float d;
    printf("Enter the co-ordinates of point 1:");
    scanf("%f%f",&d1.x,&d1.y);
    printf("Enter the co-ordinates of point 2:");
    scanf("%f%f",&d2.x,&d2.y);
    d=sqrt(pow((d1.x-d2.x),2)+pow((d1.y-d2.y),2));
    printf("Distance = %f",d);
    return 0;
}